package com.example.tihon.dagger2example.ui.info;


import com.example.tihon.dagger2example.data.InfoRepository;

public class InfoPresenter {

    final InfoRepository infoRepository;
    final View view;


    public InfoPresenter(View view, InfoRepository infoRepository) {
        this.infoRepository = infoRepository;
        this.view = view;
    }

    public void start() {
        view.onSuccess(infoRepository.getSuccessString());
        view.onResourceString(infoRepository.getResourceString());
    }

    interface View {
        void onSuccess(String s);
        void onResourceString(String s);
    }

}
