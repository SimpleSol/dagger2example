package com.example.tihon.dagger2example.ui.fragments.one;


import com.example.tihon.dagger2example.data.FragmentsInfo;
import com.example.tihon.dagger2example.data.OneRepository;

import javax.inject.Inject;

public class OnePresenter {

    final View view;
    final OneRepository oneRepository;
    final FragmentsInfo fragmentsInfo;

    @Inject
    public OnePresenter(View view, OneRepository oneRepository, FragmentsInfo fragmentsInfo) {
        this.view = view;
        this.oneRepository = oneRepository;
        this.fragmentsInfo = fragmentsInfo;
    }

    public void start() {
        view.onOneRepositoryInfo(String.valueOf(oneRepository.getDoubledVersionCode()));
        view.onOneFragmentInfo(fragmentsInfo.getOneFragmentInfo());
    }

    interface View {
        void onOneRepositoryInfo(String s);
        void onOneFragmentInfo(String s);
    }

}
