package com.example.tihon.dagger2example.data;


import android.content.Context;
import android.content.res.Resources;

import com.example.tihon.dagger2example.R;

public class InfoRepositoryImpl implements InfoRepository {

    final Resources resources;

    public InfoRepositoryImpl(Context context) {
        resources = context.getResources();
    }

    @Override
    public String getSuccessString() {
        return "success";
    }

    @Override
    public String getResourceString() {
        return resources.getString(R.string.string_from_res);
    }
}
