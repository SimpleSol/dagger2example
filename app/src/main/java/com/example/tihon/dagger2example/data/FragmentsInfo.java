package com.example.tihon.dagger2example.data;


public class FragmentsInfo {

    public String getOneFragmentInfo() {
        return "one fragment info";
    }

    public String getTwoFragmentInfo() {
        return "two fragment info";
    }

}
