package com.example.tihon.dagger2example.ui.info;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.tihon.dagger2example.R;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class InfoActivity extends DaggerAppCompatActivity implements InfoPresenter.View {

    @Inject InfoPresenter infoPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        infoPresenter.start();
    }

    @Override
    public void onSuccess(String s) {
        Log.d("33__", s);
    }

    @Override
    public void onResourceString(String s) {
        Log.d("33__", s);
    }
}
