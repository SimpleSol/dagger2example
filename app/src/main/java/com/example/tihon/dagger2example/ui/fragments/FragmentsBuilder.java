package com.example.tihon.dagger2example.ui.fragments;


import com.example.tihon.dagger2example.ui.fragments.one.OneFragment;
import com.example.tihon.dagger2example.ui.fragments.one.OneFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentsBuilder {

    @ContributesAndroidInjector(modules = OneFragmentModule.class)
    abstract OneFragment provideOneFragment();

}
