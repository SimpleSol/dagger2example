package com.example.tihon.dagger2example.ui.info;

import com.example.tihon.dagger2example.data.InfoRepository;
import com.example.tihon.dagger2example.data.InfoRepositoryImpl;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class InfoActivityModule {
    @Provides
    static InfoPresenter provideInfoPresenter(InfoPresenter.View view, InfoRepository infoRepository) {
        return new InfoPresenter(view, infoRepository);
    }

    @Provides
    static InfoRepository provideInfoRepository(InfoActivity infoActivity) {
        return new InfoRepositoryImpl(infoActivity);
    }

    @Binds
    abstract InfoPresenter.View provideInfoView(InfoActivity infoActivity);
}
