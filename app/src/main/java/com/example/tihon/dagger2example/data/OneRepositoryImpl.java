package com.example.tihon.dagger2example.data;


import javax.inject.Inject;

public class OneRepositoryImpl implements OneRepository {

    final AppInfo appInfo;

    @Inject
    public OneRepositoryImpl(AppInfo appInfo) {
        this.appInfo = appInfo;
    }


    @Override
    public int getDoubledVersionCode() {
        return appInfo.getVersionCode() * 2;
    }
}
