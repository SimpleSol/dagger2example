package com.example.tihon.dagger2example.data;


public interface InfoRepository {
    String getSuccessString();
    String getResourceString();
}
