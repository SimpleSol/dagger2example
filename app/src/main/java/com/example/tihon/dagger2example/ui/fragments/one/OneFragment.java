package com.example.tihon.dagger2example.ui.fragments.one;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tihon.dagger2example.L;
import com.example.tihon.dagger2example.R;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class OneFragment extends DaggerFragment implements OnePresenter.View {

    @Inject OnePresenter onePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onePresenter.start();
    }

    @Override
    public void onOneRepositoryInfo(String s) {
        L.d(s);
    }

    @Override
    public void onOneFragmentInfo(String s) {
        L.d(s);
    }
}
