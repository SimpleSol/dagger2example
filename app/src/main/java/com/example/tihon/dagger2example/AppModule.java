package com.example.tihon.dagger2example;


import com.example.tihon.dagger2example.data.AppInfo;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    AppInfo provideAppInfo() {
        return new AppInfo();
    }

}
