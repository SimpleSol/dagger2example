package com.example.tihon.dagger2example.ui.fragments.one;


import com.example.tihon.dagger2example.data.AppInfo;
import com.example.tihon.dagger2example.data.FragmentsInfo;
import com.example.tihon.dagger2example.data.OneRepository;
import com.example.tihon.dagger2example.data.OneRepositoryImpl;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class OneFragmentModule {

    @Provides
    static OnePresenter provideOnePresenter(OnePresenter.View view, OneRepository oneRepository, FragmentsInfo fragmentsInfo) {
        return new OnePresenter(view, oneRepository, fragmentsInfo);
    }

    @Provides
    static OneRepository provideOneRepository(AppInfo appInfo) {
        return new OneRepositoryImpl(appInfo);
    }

    @Binds
    abstract OnePresenter.View provideOneView(OneFragment fragment);

}
