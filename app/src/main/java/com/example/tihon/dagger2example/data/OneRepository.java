package com.example.tihon.dagger2example.data;


public interface OneRepository {

    int getDoubledVersionCode();

}
