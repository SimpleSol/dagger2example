package com.example.tihon.dagger2example.ui.fragments;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tihon.dagger2example.ui.fragments.one.OneFragment;
import com.example.tihon.dagger2example.ui.fragments.two.TwoFragment;

public class FragmentsAdapter extends FragmentPagerAdapter {

    private final String[] titles;

    public FragmentsAdapter(FragmentManager fm) {
        super(fm);
        titles = new String[] {"One", "Two"};
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new OneFragment();
        } else if (position == 1) {
            return new TwoFragment();
        } else {
            return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}
