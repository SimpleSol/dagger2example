package com.example.tihon.dagger2example.data;


public class AppInfo {

    public String getAppName() {
        return "Dagger 2 Example";
    }

    public String getAppVersion() {
        return "1.0.0";
    }

    public int getVersionCode() {
        return 1;
    }

}
