package com.example.tihon.dagger2example.ui.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.example.tihon.dagger2example.data.AppInfo;
import com.example.tihon.dagger2example.R;
import com.example.tihon.dagger2example.ui.fragments.FragmentsActivity;
import com.example.tihon.dagger2example.ui.info.InfoActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements View.OnClickListener {

    private static final String TAG = "33__";

    @Inject AppInfo appInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnInfo).setOnClickListener(this);
        findViewById(R.id.btnFragments).setOnClickListener(this);
        Log.d(TAG, appInfo == null ? "fail" : appInfo.getAppName());
    }

    @Override
    public void onClick(View v) {
        Class klass;
        int id = v.getId();
        if (id == R.id.btnInfo) {
            klass = InfoActivity.class;
        } else if (id == R.id.btnFragments) {
            klass = FragmentsActivity.class;
        } else {
            klass = InfoActivity.class;
        }
        startActivity(new Intent(this, klass));
    }


}
