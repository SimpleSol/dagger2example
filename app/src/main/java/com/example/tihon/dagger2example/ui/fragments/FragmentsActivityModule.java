package com.example.tihon.dagger2example.ui.fragments;


import com.example.tihon.dagger2example.data.FragmentsInfo;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentsActivityModule {

    @Provides
    public FragmentsInfo provideFragmentsInfo() {
        return new FragmentsInfo();
    }

}
