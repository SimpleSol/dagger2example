package com.example.tihon.dagger2example;


import com.example.tihon.dagger2example.ui.fragments.FragmentsActivity;
import com.example.tihon.dagger2example.ui.fragments.FragmentsActivityModule;
import com.example.tihon.dagger2example.ui.fragments.FragmentsBuilder;
import com.example.tihon.dagger2example.ui.info.InfoActivity;
import com.example.tihon.dagger2example.ui.info.InfoActivityModule;
import com.example.tihon.dagger2example.ui.main.MainActivity;
import com.example.tihon.dagger2example.ui.main.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = InfoActivityModule.class)
    abstract InfoActivity bindInfoActivity();

    @ContributesAndroidInjector(modules = {FragmentsActivityModule.class, FragmentsBuilder.class})
    abstract FragmentsActivity bindFragmentsActivity();

}
